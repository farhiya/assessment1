# Assessment 1 README

In this assessment I created 3 instances....


## Creating Ubuntu instance

1. first log in to aws account
2. ensure you are in your user and not google... (check top right corner)
3. search EC2 in search bar
4. On the left pannel click 'Instances'
5. click the 'Launch Instance' button on the upper left hand side of the page (Its in orange)
6. Select Ubuntu server 18.04
    - Ensure you select the free option that has 1 vCPUs and 1 GiB memory then click Next
    - Ensure Auto-assign Public IP is on Enable, click Next
    - click Next again (Do not change anything in storage)
    - click 'Add Tag button. In Key enter 'Name' and Value enter <myname-ubuntu>
    - Add another tag. In Key enter 'Project' and Value enter assessment1. 
    - Click Next
    - Select 'Create a new security group'. and give it a meaningful name and description.
    - In the first rule, change Source from 'Custom' to 'Mp IP'.
    - Click Add Rule.
    - Select Type as 'HTTP' and change Source from 'Custom' to 'Anywhere'
    - Click review and launch button
    - Click Launch
    - You will be prompted to create ane key pair or use an existing one. Create a new key pair. Select RSA and give it a meaningful name.
    - Download the key pair
    - click Launch and view your instance.
7. Move Key from Downloads to .ssh file
8. login to ubuntu machine with public ip of this instance
    - 'ssh -o StrictHostKeyChecking=no -i ~/.ssh/<key_name>.pem ubuntu@<ip_address>'
9.  update packages
    - 'sudo apt-get update'
10. install nginx
    - 'sudo apt install nginx -y'
11. start nginx
    - 'sudo systemctl start nginx'
12. Create instructor user
    - 'sudo adduser instructor'
    - enter password
    - not neccessary to fill in other details, just keep presssing enter then y

## Creating Linux instance

1. click the 'Launch Instance' button on the upper left hand side of the page (Its in orange)
2. Select Amazon Linux 2 AMI (HMV) latest version
    - Ensure you select the free option that has 1 vCPUs and 1 GiB memory then click Next
    - Ensure Auto-assign Public IP is on Enable, click Next
    - click Next again (Do not change anything in storage)
    - click 'Add Tag button. In Key enter 'Name' and Value enter <myname-aws>
    - Add another tag. In Key enter 'Project' and Value enter assessment1. 
    - Click Next
    - Select 'Create a new security group'. and give it a meaningful name and description.
    - In the first rule, change Source from 'Custom' to 'Mp IP'.
    - Click Add Rule.
    - Select Type as 'HTTP' and change Source from 'Custom' to 'Anywhere'
    - Click review and launch button
    - Click Launch
    - You will be prompted to create ane key pair or use an existing one. Use existing key pair you just created.
    - click Launch and view your instance.
3. login to linux machine
    - 'ssh -o StrictHostKeyChecking=no -i ~/.ssh/<key_name>.pem ec2-user@<ip_address>'
4. Update all packages
   - 'sudo yum update -y'
5. install apache
    - 'sudo yum install -y httpd.x86_64'
6. start apache
    - 'sudo systemctl start httpd.service'
7. Create instructor user
    - 'sudo adduser instructor'
    - enter password
    - not neccessary to fill in other details, just keep presssing enter then y

   
## Creating Load balancer

1. For my Load balancer I created in an ubuntu machine. follow 'creating ubuntu instance' but do not create a new key, use the key you created originally.

2. login to ubuntu machine 
3. install HA Proxy
    - 'sudo apt update'
    - 'sudo apt install -y haproxy'
4. configure ha proxy
    - 'sudo nano /etc/haproxy/haproxy.cfg'
    - config.haproxy is inside this nano
5. restart ha proxy
   - 'sudo systemctl restart haproxy'
6. open puplic ip for load balancer in browser
    - nginx welcome page should appear
    - refresh page
    - apache welcom page should appear


Currently getting error 503 web page. Trying to debug.
When i check status it tells me in Docs: a file is running in a different path. Can't change it to where my config is.